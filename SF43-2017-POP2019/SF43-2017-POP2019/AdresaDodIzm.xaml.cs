﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF43_2017_POP2019.Model;

namespace SF43_2017_POP2019
{
    
    public partial class AdresaDodIzm : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Adresa adresa;
        public AdresaDodIzm(Adresa adresa, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.adresa = adresa;
            this.opcija = opcija;

            this.DataContext = adresa;

            if (opcija == EOpcija.IZMENA)
            {
                TxtId.IsEnabled = false;
            }
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                adresa.SacuvajAdresu();
            }
            if (opcija.Equals(EOpcija.IZMENA))
            {
                adresa.IzmenaAdrese();
            }
        }
        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
