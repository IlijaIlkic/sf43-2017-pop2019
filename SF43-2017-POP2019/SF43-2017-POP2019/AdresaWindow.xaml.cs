﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF43_2017_POP2019.Data;
using SF43_2017_POP2019.Model;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for AdresaWindow.xaml
    /// </summary>
    public partial class AdresaWindow : Window
    {
        ICollectionView view;
        public AdresaWindow()
        {
            InitializeComponent();
            DGAdresa.ItemsSource = Data.Data.Instance.Adrese ;
            DGAdresa.IsSynchronizedWithCurrentItem = true;

            DGAdresa.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.Adrese);
       
        
        

        }
    
        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnDodajAdresu_Click(object sender, RoutedEventArgs e)
        {
            AdresaDodIzm ad = new AdresaDodIzm(new Adresa());
            ad.ShowDialog();
        }

        private void BtnIzmeniAdresu_Click(object sender, RoutedEventArgs e)
        {
            Adresa selektovanaAdresa = (Adresa)DGAdresa.SelectedItem;
            if (selektovanaAdresa != null)
            {
                Adresa stari = (Adresa)selektovanaAdresa.Clone();
                AdresaDodIzm ai = new AdresaDodIzm(selektovanaAdresa, AdresaDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksAdresa(selektovanaAdresa.Id.ToString());
                    Data.Data.Instance.Adrese[indeks] = stari;
                }
                else
                {
                    selektovanaAdresa.IzmenaAdrese();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovana nijedna adresa");
            }
            DGAdresa.Items.Refresh();
        }

        private void BtnObrisiAdresu_Click(object sender, RoutedEventArgs e)
        {
            Adresa adresa = (Adresa)DGAdresa.SelectedItem;
            if (SelektovanaAdresa(adresa) && adresa.Aktivan) 
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksAdresa(adresa.Id.ToString());
                    Data.Data.Instance.Adrese[indeks].Aktivan = false;
                    Adresa selektovanaAdresa = (Adresa)DGAdresa.SelectedItem;
                    Data.Data.Instance.Adrese.Remove(selektovanaAdresa);

                    selektovanaAdresa.IzmenaAdrese();
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksAdresa(String id)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.Adrese.Count; i++)
            {
                if (Data.Data.Instance.Adrese[i].Id.Equals(id))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanaAdresa(Adresa adresa)
        {
            if (adresa == null)
            {
                MessageBox.Show("Nije selektovana adresa! ");
                return false;
            }
            return true;
        }

       
    }
}
