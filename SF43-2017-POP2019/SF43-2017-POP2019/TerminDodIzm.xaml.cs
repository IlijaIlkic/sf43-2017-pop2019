﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for TerminDodIzm.xaml
    /// </summary>
    public partial class TerminDodIzm : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Termin termin;
        public TerminDodIzm(Termin termin, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.termin = termin;
            this.opcija = opcija;

            this.DataContext = termin;

            //dpDatumTermina.SelectedDateFormat = DatePickerFormat.Short;

            foreach (Lekar lekar in Data.Data.Instance.Lekari)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = lekar.Korisnik.ImePrezime;
                item.Value = lekar.Korisnik.Jmbg;

                cbZakazanLekar.Items.Add(item);

                if (termin.LekarZakazan != null && termin.LekarZakazan.Korisnik.Jmbg == item.Value)
                {
                    cbZakazanLekar.SelectedItem = item;
                }
            }

            foreach (RegistrovaniKorisnik pacijent in Data.Data.Instance.Pacijenti)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = pacijent.ImePrezime;
                item.Value = pacijent.Jmbg;

                cbPacijent.Items.Add(item);

                if (termin.Pacijent != null && termin.Pacijent.Jmbg == item.Value)
                {
                    cbPacijent.SelectedItem = item;
                }
            }

            rbSlobodan.IsChecked = termin.StatusTermina == Termin.EStatusTermina.SLOBODAN;
            rbZakazan.IsChecked = termin.StatusTermina == Termin.EStatusTermina.ZAKAZAN;

        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            //DateTime datum = (DateTime)dpDatumTermina.SelectedDate;
            termin.DatumTermina = DateTime.Now;

            this.termin.LekarZakazan = Data.Data.Instance.Lekari.First(lekar => lekar.Korisnik.Jmbg == ((ComboboxItem)cbZakazanLekar.SelectedValue).Value);
            this.termin.Pacijent = Data.Data.Instance.RegistrovaniKorisnici.First(korisnik => korisnik.Jmbg == ((ComboboxItem)cbPacijent.SelectedValue).Value);


            if (rbSlobodan.IsChecked.Equals(true))
            {
                termin.StatusTermina = Termin.EStatusTermina.SLOBODAN;
            }
            else
            {
                termin.StatusTermina = Termin.EStatusTermina.ZAKAZAN;
            }


            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                termin.SacuvajTermin();
            }

            if (opcija.Equals(EOpcija.IZMENA))
            {
                termin.IzmenaTermina();
            }
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
