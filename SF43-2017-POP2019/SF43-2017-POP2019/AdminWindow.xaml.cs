﻿using SF43_2017_POP2019.Model;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        ICollectionView view;
        public AdminWindow()
        {
            InitializeComponent();
            DGAdmin.ItemsSource = Data.Data.Instance.Administratori;
            DGAdmin.IsSynchronizedWithCurrentItem = true;

            DGAdmin.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.RegistrovaniKorisnici);
            
        }

        

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnDodajAdmina_Click(object sender, RoutedEventArgs e)
        {
            AdminDodIzm ad = new AdminDodIzm(new RegistrovaniKorisnik());
            ad.ShowDialog();

        }

        private void btnIzmeniAdmina_Click(object sender, RoutedEventArgs e)
        {

           
            RegistrovaniKorisnik selektovanAdmin = (RegistrovaniKorisnik)DGAdmin.SelectedItem;
            if (selektovanAdmin != null)
            {
                RegistrovaniKorisnik stari = (RegistrovaniKorisnik)selektovanAdmin.Clone();
                AdminDodIzm ai = new AdminDodIzm(selektovanAdmin, AdminDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksAdmina(selektovanAdmin.Jmbg.ToString());
                    Data.Data.Instance.RegistrovaniKorisnici[indeks] = stari;
                }
                else
                {
                    selektovanAdmin.IzmenaRegistrovanogKorisnika();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan dom zdravlja");
            }
            DGAdmin.Items.Refresh();
           
        }



        private void btnObrisiAdmina_Click(object sender, RoutedEventArgs e)
        {
            RegistrovaniKorisnik registrovaniKorisnik= (RegistrovaniKorisnik)DGAdmin.SelectedItem;
            if (SelektovanAdmin(registrovaniKorisnik) && registrovaniKorisnik.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksAdmina(registrovaniKorisnik.Jmbg.ToString());
                    Data.Data.Instance.RegistrovaniKorisnici[indeks].Aktivan = false;
                    RegistrovaniKorisnik selektovanAdmin = (RegistrovaniKorisnik)DGAdmin.SelectedItem;
                    Data.Data.Instance.RegistrovaniKorisnici.Remove(selektovanAdmin);

                    selektovanAdmin.IzmenaRegistrovanogKorisnika();
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksAdmina(String jmbg)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.RegistrovaniKorisnici.Count; i++)
            {
                if (Data.Data.Instance.RegistrovaniKorisnici[i].Jmbg.Equals(jmbg))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanAdmin(RegistrovaniKorisnik registrovaniKorisnik)
        {
            if (registrovaniKorisnik == null)
            {
                MessageBox.Show("Nije selektovan dom zdravlja! ");
                return false;
            }
            return true;
        }


    }
}
