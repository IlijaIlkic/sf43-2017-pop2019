﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for LekarWindow.xaml
    /// </summary>
    
    public partial class LekarWindow : Window
    {
        ICollectionView view; 
        public LekarWindow()
        {
            InitializeComponent();
            DGLekar.ItemsSource = Data.Data.Instance.Lekari;
            DGLekar.IsSynchronizedWithCurrentItem = true;

            DGLekar.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.Lekari);
            
        }
       
    

    private void btnDodajLekara_Click(object sender, RoutedEventArgs e)
        {
            LekarDodIzm ad = new LekarDodIzm(new Lekar());
            ad.ShowDialog();


        }

        private void btnIzmeniLekara_Click(object sender, RoutedEventArgs e)
        {

            Lekar selektovanLekar = (Lekar)DGLekar.SelectedItem;
            if (selektovanLekar != null)
            {
                Lekar stari = (Lekar) selektovanLekar.Clone();
                LekarDodIzm ai = new LekarDodIzm(selektovanLekar, LekarDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksLekar(selektovanLekar.Korisnik.Aktivan.ToString());
                    Data.Data.Instance.Lekari[indeks] = stari;
                }
                else
                {
                    selektovanLekar.IzmenaLekara();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan lekar");
            }

            DGLekar.Items.Refresh();


        }

        private void btnObrisiLekara_Click(object sender, RoutedEventArgs e)
        {

            Lekar lekar = (Lekar)DGLekar.SelectedItem;
            if (SelektovanLekar(lekar) && lekar.Korisnik.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    lekar.Korisnik.Aktivan = false;
                    lekar.IzmenaLekara();
                }
            }

        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private int IndeksLekar(String jmbg)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.RegistrovaniKorisnici.Count; i++)
            {
                if (Data.Data.Instance.RegistrovaniKorisnici[i].Jmbg.Equals(jmbg))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanLekar(Lekar lekar)
        {
            if (lekar == null)
            {
                MessageBox.Show("Nije selektovan lekar! ");
                return false;
            }
            return true;
        }
    }
}
