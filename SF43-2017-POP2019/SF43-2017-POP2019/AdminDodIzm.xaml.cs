﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SF43_2017_POP2019.Model.RegistrovaniKorisnik;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for AdminDodIzm.xaml
    /// </summary>
    public partial class AdminDodIzm : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;        
        private RegistrovaniKorisnik registrovaniKorisnik;
    
        public AdminDodIzm(RegistrovaniKorisnik registrovaniKorisnik, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.registrovaniKorisnik = registrovaniKorisnik;
            this.opcija = opcija;

            this.DataContext = registrovaniKorisnik;


            cbPol.ItemsSource = Data.Data.Instance.RegistrovaniKorisnici;

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtJmbg.IsEnabled = false;
            }

            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = adresa.UlicaBroj;
                item.Value = adresa.Id;

                cbAdresa.Items.Add(item);

                if (registrovaniKorisnik.Adresa != null && registrovaniKorisnik.Adresa.Id == item.Value)
                {
                    cbAdresa.SelectedItem = item;
                }
            }


        }


        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                registrovaniKorisnik.SacuvajRegistrovanogKorinsika();
            }

            if (opcija.Equals(EOpcija.IZMENA))
            {
                registrovaniKorisnik.IzmenaRegistrovanogKorisnika();
            }
        }


        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
