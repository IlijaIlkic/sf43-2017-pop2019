﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for PacijentWindow.xaml
    /// </summary>
    public partial class PacijentWindow : Window
    {
        ICollectionView view;
        public PacijentWindow()
        {
            InitializeComponent();
            DGPacijent.ItemsSource = Data.Data.Instance.Pacijenti;
            DGPacijent.IsSynchronizedWithCurrentItem = true;

            DGPacijent.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.RegistrovaniKorisnici);
            
            
        }
   
        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void BtnDodajPacijenta_Click(object sender, RoutedEventArgs e)
        {
            PacijentDodIzm ad = new PacijentDodIzm(new RegistrovaniKorisnik());
            ad.ShowDialog();

        }

        private void BtnIzmeniPacijenta_Click(object sender, RoutedEventArgs e)
        { 
            
            RegistrovaniKorisnik selektovanPacijent = (RegistrovaniKorisnik)DGPacijent.SelectedItem;
            if (selektovanPacijent != null)
            {
                RegistrovaniKorisnik stari = (RegistrovaniKorisnik)selektovanPacijent.Clone();
                PacijentDodIzm ai = new PacijentDodIzm(selektovanPacijent, PacijentDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksPacijent(selektovanPacijent.Aktivan.ToString());
                    Data.Data.Instance.Pacijenti[indeks] = stari;
                }
                else
                {
                    selektovanPacijent.IzmenaRegistrovanogKorisnika();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan pacijent");
            }

            DGPacijent.Items.Refresh();
           
        }

        private void BtnObrisiPacijenta_Click(object sender, RoutedEventArgs e)
        {

            RegistrovaniKorisnik pacijent = (RegistrovaniKorisnik)DGPacijent.SelectedItem;
            if (SelektovanPacijent(pacijent) && pacijent.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksPacijent(pacijent.Jmbg.ToString());
                    Data.Data.Instance.Pacijenti[indeks].Aktivan = false;
                    RegistrovaniKorisnik selektovanPacijent = (RegistrovaniKorisnik)DGPacijent.SelectedItem;
                    Data.Data.Instance.Pacijenti.Remove(selektovanPacijent);

                    selektovanPacijent.IzmenaRegistrovanogKorisnika();
                }
            }
            
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksPacijent(String jmbg)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.Pacijenti.Count; i++)
            {
                if (Data.Data.Instance.Pacijenti[i].Jmbg.Equals(jmbg))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanPacijent(RegistrovaniKorisnik pacijent)
        {
            if (pacijent == null)
            {
                MessageBox.Show("Nije selektovan pacijent! ");
                return false;
            }
            return true;
        }


    }
}
