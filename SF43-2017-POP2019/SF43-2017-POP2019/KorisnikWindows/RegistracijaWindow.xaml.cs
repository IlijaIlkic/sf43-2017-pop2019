﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019.KorisnikWindows
{
    /// <summary>
    /// Interaction logic for RegistracijaWindow.xaml
    /// </summary>
    public partial class RegistracijaWindow : Window
    {

        public RegistrovaniKorisnik registrovaniKorisnik;
        
        public RegistracijaWindow()
        {
            InitializeComponent();
            this.registrovaniKorisnik = new RegistrovaniKorisnik();
            

            this.DataContext = registrovaniKorisnik;


            cbPol.Items.Add(RegistrovaniKorisnik.EPol.MUSKI);
            cbPol.Items.Add(RegistrovaniKorisnik.EPol.ZENSKI);

           
           
            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = adresa.UlicaBroj;
                item.Value = adresa.Id;

                cbAdresa.Items.Add(item);

                if (adresa.Id == item.Value)
                {
                    cbAdresa.SelectedItem = item;
                }
            }
         
         
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.registrovaniKorisnik.Adresa = Data.Data.Instance.Adrese.First(adresa => adresa.Id == ((ComboboxItem)cbAdresa.SelectedItem).Value);
            this.registrovaniKorisnik.TipK = RegistrovaniKorisnik.ETipKorisnika.PACIJENT;

           
            
             registrovaniKorisnik.SacuvajRegistrovanogKorinsika();
           


        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }
    }
}
