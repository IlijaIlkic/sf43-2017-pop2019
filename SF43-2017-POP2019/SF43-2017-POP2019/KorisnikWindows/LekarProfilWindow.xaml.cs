﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019.KorisnikWindows
{
    /// <summary>
    /// Interaction logic for LekarProfilWindow.xaml
    /// </summary>
    public partial class LekarProfilWindow : Window
    {
        private Lekar lekar;

        public LekarProfilWindow()
        {
            InitializeComponent();
            this.lekar = Data.Data.Instance.Lekari.First(lekar => lekar.Korisnik.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg);

            this.DataContext = lekar;

            foreach (DomZdravlja domZdravlja in Data.Data.Instance.DomoviZdravlja)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = domZdravlja.NazivInst;
                item.Value = domZdravlja.Sifra;
                cbDomZdravlja.Items.Add(item);

                if (lekar.DomZdravlja != null && lekar.DomZdravlja.Sifra == item.Value)
                {
                    cbDomZdravlja.SelectedItem = item;
                }
            }

            cbPol.Items.Add(RegistrovaniKorisnik.EPol.MUSKI);
            cbPol.Items.Add(RegistrovaniKorisnik.EPol.ZENSKI);

            txtJmbg.IsEnabled = false;

            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = adresa.UlicaBroj;
                item.Value = adresa.Id;

                cbAdresa.Items.Add(item);

                if (lekar.Korisnik != null && lekar.Korisnik.Adresa != null && lekar.Korisnik.Adresa.Id == item.Value)
                {
                    cbAdresa.SelectedItem = item;
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            KorisnikWindows.LekarGlavniWindow mw = new KorisnikWindows.LekarGlavniWindow();
            mw.ShowDialog();
            return;
        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            this.lekar.DomZdravlja = Data.Data.Instance.DomoviZdravlja.First(dz => dz.Sifra == ((ComboboxItem)cbDomZdravlja.SelectedValue).Value);
            this.lekar.Korisnik.Adresa = Data.Data.Instance.Adrese.First(adresa => adresa.Id == ((ComboboxItem)cbAdresa.SelectedValue).Value);
            lekar.IzmenaLekara();
            MessageBox.Show("Uspesno ste promenili podatke");
        }
    }
}
