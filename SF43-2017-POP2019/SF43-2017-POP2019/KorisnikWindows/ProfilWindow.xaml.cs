﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019.KorisnikWindows
{
    /// <summary>
    /// Interaction logic for ProfilWindow.xaml
    /// </summary>
    public partial class ProfilWindow : Window
    {
        public RegistrovaniKorisnik korisnikUlog;
        public ProfilWindow()
        {
            InitializeComponent();
            this.korisnikUlog = Data.Data.Instance.ulogovaniKorisnik;

            this.DataContext = korisnikUlog;

            cbPol.Items.Add(RegistrovaniKorisnik.EPol.MUSKI);
            cbPol.Items.Add(RegistrovaniKorisnik.EPol.ZENSKI);

            txtJmbg.IsEnabled = false;

            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = adresa.UlicaBroj;
                item.Value = adresa.Id;

                cbAdresa.Items.Add(item);

                if (korisnikUlog.Adresa != null && korisnikUlog.Adresa.Id == item.Value)
                {
                    cbAdresa.SelectedItem = item;
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnIzmena_Click(object sender, RoutedEventArgs e)
        {
            this.korisnikUlog.Adresa = Data.Data.Instance.Adrese.First(adresa => adresa.Id == ((ComboboxItem)cbAdresa.SelectedValue).Value);

            if (korisnikUlog.TipK.Equals(RegistrovaniKorisnik.ETipKorisnika.ADMINISTRATOR))
            {
                korisnikUlog.IzmenaRegistrovanogKorisnika();

            }
            else if (korisnikUlog.TipK.Equals(RegistrovaniKorisnik.ETipKorisnika.PACIJENT))
            {
                korisnikUlog.IzmenaRegistrovanogKorisnika();
            }

            MessageBox.Show("Uspesno ste promenili podatke");
        }
    }
}
