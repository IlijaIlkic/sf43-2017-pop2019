﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019.KorisnikWindows
{
    /// <summary>
    /// Interaction logic for PacijentGlavniWindow.xaml
    /// </summary>
    /// 
    public partial class PacijentGlavniWindow : Window
    {
        public RegistrovaniKorisnik korisnikUlog;

        public PacijentGlavniWindow()
        {
            InitializeComponent();
            this.korisnikUlog = Data.Data.Instance.ulogovaniKorisnik;
            txtUlogovaniK.Text = korisnikUlog.ImePrezime;
        }

        private void btnPregledProfila_Click(object sender, RoutedEventArgs e)
        {
            KorisnikWindows.ProfilWindow ad = new KorisnikWindows.ProfilWindow();
            ad.ShowDialog();


        }

        private void btnKarton_Click(object sender, RoutedEventArgs e)
        {
            TerapijaWindow ad = new TerapijaWindow();
            ad.ShowDialog();
        }

        private void btnDomoviZdravlja_Click(object sender, RoutedEventArgs e)
        {
            DomZdravljaWindow ad = new DomZdravljaWindow();
            ad.ShowDialog();


        }

        private void btnLekari_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnOdjava_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow ad = new LoginWindow();
            ad.ShowDialog();


        }
    }
}
