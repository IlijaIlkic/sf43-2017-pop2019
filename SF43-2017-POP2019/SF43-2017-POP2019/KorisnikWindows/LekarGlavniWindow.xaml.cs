﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019.KorisnikWindows
{
    /// <summary>
    /// Interaction logic for LekarGlavniWindow.xaml
    /// </summary>
    public partial class LekarGlavniWindow : Window
    {
        public Lekar lekar;

        public LekarGlavniWindow()
        {
            InitializeComponent();
            this.lekar = Data.Data.Instance.Lekari.First(lekar => lekar.Korisnik.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg);
            txtUlogovaniK.Text = lekar.Korisnik.ImePrezime;
        }

        private void btnPregledProfila_Click(object sender, RoutedEventArgs e)
        {
            KorisnikWindows.LekarProfilWindow ad = new KorisnikWindows.LekarProfilWindow();
            ad.ShowDialog();


        }

        private void btnKarton_Click(object sender, RoutedEventArgs e)
        {
            TerapijaWindow ad = new TerapijaWindow();
            ad.ShowDialog();

        }

        private void btnDomoviZdravlja_Click(object sender, RoutedEventArgs e)
        {
            DomZdravljaWindow ad = new DomZdravljaWindow();
            ad.ShowDialog();

        }

        private void btnTerapije_Click(object sender, RoutedEventArgs e)
        {
            TerapijaWindow ad = new TerapijaWindow();
            ad.ShowDialog();


        }

        private void btnTermini_Click(object sender, RoutedEventArgs e)
        {
            TerminiWindow ad = new TerminiWindow();
            ad.ShowDialog();

        }

        private void btnOdjava_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow ad = new LoginWindow();
            ad.ShowDialog();

        }
    }
}
