﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for DomZdravljaDodIzm.xaml
    /// </summary>
    public partial class DomZdravljaDodIzm : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private DomZdravlja domZdravlja;
        public DomZdravljaDodIzm(DomZdravlja domZdravlja, EOpcija opcija = EOpcija.DODAVANJE)
        {
            
            InitializeComponent();
            this.domZdravlja = domZdravlja;
            this.opcija = opcija;

            this.DataContext = domZdravlja;

            

            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = adresa.UlicaBroj;
                item.Value = adresa.Id;

                cbAdresa.Items.Add(item);

                if (domZdravlja.Adresa!= null && domZdravlja.Adresa.Id == item.Value)
                {
                    cbAdresa.SelectedItem = item;
                }
            }

        }



        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                this.domZdravlja.Adresa = Data.Data.Instance.Adrese.First(adresa => adresa.Id== ((ComboboxItem)cbAdresa.SelectedValue).Value);
                domZdravlja.SacuvajDomZdravlja();
            }

            if (opcija.Equals(EOpcija.IZMENA))
            {
                this.domZdravlja.Adresa = Data.Data.Instance.Adrese.First(adresa => adresa.Id == ((ComboboxItem)cbAdresa.SelectedValue).Value);
                domZdravlja.IzmenaDomaZdravlja();
            }
        }
        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
