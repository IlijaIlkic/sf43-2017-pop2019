﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for TerapijaDodIzm.xaml
    /// </summary>
    public partial class TerapijaDodIzm : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Terapija terapija;
        public TerapijaDodIzm(Terapija terapija, EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();
            this.terapija = terapija;
            this.opcija = opcija;

            this.DataContext = terapija;

            if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.LEKAR)
            {
                terapija.LekarTerapije = Data.Data.Instance.Lekari.First(lekar => lekar.Korisnik.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg);
                cbLekar.IsEnabled = false;
            }

            foreach (RegistrovaniKorisnik pacijent in Data.Data.Instance.Pacijenti)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = pacijent.ImePrezime;
                item.Value = pacijent.Jmbg;

                cbPacijent.Items.Add(item);

                if (terapija.Pacijent != null && terapija.Pacijent.Jmbg == item.Value)
                {
                    cbPacijent.SelectedItem = item;
                }
            }

            foreach (Lekar lekar in Data.Data.Instance.Lekari)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = lekar.Korisnik.ImePrezime;
                item.Value = lekar.Korisnik.Jmbg;

                cbLekar.Items.Add(item);

                if (terapija.LekarTerapije!=null && terapija.LekarTerapije.Korisnik.Jmbg == item.Value)
                {
                    cbLekar.SelectedItem = item;
                }
            }
        }

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.terapija.LekarTerapije = Data.Data.Instance.Lekari.First(lekar => lekar.Korisnik.Jmbg == ((ComboboxItem)cbLekar.SelectedValue).Value);
            this.terapija.Pacijent = Data.Data.Instance.Pacijenti.First(pacijent => pacijent.Jmbg == ((ComboboxItem)cbPacijent.SelectedValue).Value);

            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                terapija.SacuvajTerapiju();
            }

            if (opcija.Equals(EOpcija.IZMENA))
            {
                terapija.IzmenaTerapije();
            }
        }
        
        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}
