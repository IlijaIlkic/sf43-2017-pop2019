﻿#pragma checksum "..\..\DomZdravljaDodIzm.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "8DA71E770D5D6BE2EE3089E6BB9CDB5D53C25C83CFB51B95EA38A3E0B4DE86B5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using SF43_2017_POP2019;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SF43_2017_POP2019 {
    
    
    /// <summary>
    /// DomZdravljaDodIzm
    /// </summary>
    public partial class DomZdravljaDodIzm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSifra;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtSifra;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNazivInst;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxtNazivInst;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblAdresa;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbAdresa;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSacuvaj;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\DomZdravljaDodIzm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOdustani;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SF43-2017-POP2019;component/domzdravljadodizm.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\DomZdravljaDodIzm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lblSifra = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.TxtSifra = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.lblNazivInst = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.TxtNazivInst = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.lblAdresa = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.cbAdresa = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.btnSacuvaj = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\DomZdravljaDodIzm.xaml"
            this.btnSacuvaj.Click += new System.Windows.RoutedEventHandler(this.btnSacuvaj_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnOdustani = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\DomZdravljaDodIzm.xaml"
            this.btnOdustani.Click += new System.Windows.RoutedEventHandler(this.btnOdustani_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

