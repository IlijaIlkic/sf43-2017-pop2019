﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for TerminiWindow.xaml
    /// </summary>
    public partial class TerminiWindow : Window
    {
        ICollectionView view;
        public TerminiWindow()
        {
            InitializeComponent();
            DGTermin.IsSynchronizedWithCurrentItem = true;
            DGTermin.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

            if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.PACIJENT)
            {
                DGTermin.ItemsSource = Data.Data.Instance.Termini.Where(termin => termin.Pacijent.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg && termin.Aktivan);
                columnAktivan.Visibility = Visibility.Hidden;
                BtnDodajTermin.Visibility = Visibility.Hidden;
                BtnIzmeniTermin.Visibility = Visibility.Hidden;
                BtnObrisiTermin.Visibility = Visibility.Hidden;
              
            }
            else if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.LEKAR)
            {
                DGTermin.ItemsSource = Data.Data.Instance.Termini.Where(termin => termin.LekarZakazan.Korisnik.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg && termin.Aktivan);
                columnAktivan.Visibility = Visibility.Hidden;
                BtnDodajTermin.Visibility = Visibility.Hidden;
                BtnIzmeniTermin.Visibility = Visibility.Hidden;
                BtnObrisiTermin.Visibility = Visibility.Hidden;              
            }
            else if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.ADMINISTRATOR)
            {
               

                DGTermin.ItemsSource = Data.Data.Instance.Termini;               
            }

            view = CollectionViewSource.GetDefaultView(DGTermin.ItemsSource);           
        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnDodajTermin_Click(object sender, RoutedEventArgs e)
        {
            TerminDodIzm ad = new TerminDodIzm(new Termin());
            ad.ShowDialog();
        }

        private void btnIzmeniTermin_Click(object sender, RoutedEventArgs e)
        {
           
            Termin selektovanTermin = (Termin)DGTermin.SelectedItem;
            if (selektovanTermin != null)
            {
                Termin stari = (Termin)selektovanTermin.Clone();
                TerminDodIzm ai = new TerminDodIzm(selektovanTermin, TerminDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksTermina(selektovanTermin.Sifra.ToString());
                    Data.Data.Instance.Termini[indeks] = stari;
                }
                else
                {
                    selektovanTermin.IzmenaTermina();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovan nijedan termin");
            }
            DGTermin.Items.Refresh();
          
        }

        private void btnObrisiTermin_Click(object sender, RoutedEventArgs e)
        {
           
            Termin termin= (Termin)DGTermin.SelectedItem;
            if (SelektovanTermin(termin) && termin.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksTermina(termin.Sifra.ToString());
                    Data.Data.Instance.Termini[indeks].Aktivan = false;
                    Termin selektovaniTermin = (Termin)DGTermin.SelectedItem;
                    Data.Data.Instance.Termini.Remove(selektovaniTermin);

                    selektovaniTermin.IzmenaTermina();
                }
            
            }
            
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksTermina(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.Termini.Count; i++)
            {
                if (Data.Data.Instance.Termini[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanTermin(Termin termin)
        {
            if (termin == null)
            {
                MessageBox.Show("Nije selektovan termin! ");
                return false;
            }
            return true;
        }


    }
}
