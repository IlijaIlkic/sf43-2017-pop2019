﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SF43_2017_POP2019.Model
{
    public class Termin : INotifyPropertyChanged, ICloneable
    {
        public Termin()
        {
            aktivan = true;
        }

        public enum EStatusTermina { SLOBODAN, ZAKAZAN };

        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private Lekar lekarZakazan;

        public Lekar LekarZakazan
        {
            get { return lekarZakazan; }
            set { lekarZakazan = value; OnProperyChanged("LekarZakazan"); }
        }

        private DateTime datumTermina;

        public DateTime DatumTermina
        {
            get { return datumTermina; }
            set { datumTermina = value; OnProperyChanged("DatumTermina"); }
        }

        private RegistrovaniKorisnik pacijent;

        public RegistrovaniKorisnik Pacijent
        {
            get { return pacijent; }
            set { pacijent = value; OnProperyChanged("Pacijent"); }
        }


        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        private EStatusTermina statusTermina;

        public EStatusTermina StatusTermina
        {
            get { return statusTermina; }
            set { statusTermina = value; OnProperyChanged("StatusTermina"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public object Clone()
        {
            return new Termin
            {
                Sifra = this.Sifra,
                LekarZakazan = this.LekarZakazan,
                DatumTermina = this.DatumTermina,
                Pacijent = this.Pacijent,
                StatusTermina = this.StatusTermina,
                Aktivan = this.Aktivan
            };
        }
        public void SacuvajTermin()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Termin (sifra,lekar,datum,pacijent,status,aktivan)"
                                    + "VALUES(@sifra,@lekar,@datum,@pacijent,@status,@aktivan)";

                command.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@lekar", this.LekarZakazan.Korisnik.Jmbg));
                command.Parameters.Add(new SqlParameter("@datum", this.DatumTermina));
                command.Parameters.Add(new SqlParameter("@pacijent", this.Pacijent.Jmbg));
                command.Parameters.Add(new SqlParameter("@status", this.StatusTermina.ToString()));
                command.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                command.ExecuteNonQuery();
            }
            Data.Data.Instance.UcitajSveTermine();
        }
        public void IzmenaTermina()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update Termin set lekar=@lekar,pacijent=@pacijent,status=@status,datum=@datum, aktivan=@aktivan where sifra=@sifra";
                
                comm.Parameters.Add(new SqlParameter("@lekar", this.LekarZakazan.Korisnik.Jmbg));
                comm.Parameters.Add(new SqlParameter("@datum", this.DatumTermina));
                comm.Parameters.Add(new SqlParameter("@pacijent", this.Pacijent.Jmbg));
                comm.Parameters.Add(new SqlParameter("@status", this.StatusTermina.ToString()));
                comm.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));
                comm.Parameters.Add(new SqlParameter("@sifra", this.Sifra));



                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Data.Data.Instance.UcitajSveTermine();
        }
    }
}
