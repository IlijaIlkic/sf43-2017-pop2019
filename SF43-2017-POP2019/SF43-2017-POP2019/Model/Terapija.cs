﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SF43_2017_POP2019.Model
{
    public class Terapija : INotifyPropertyChanged, ICloneable
    {
        public Terapija()
        {
            aktivan = true;
        }

        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private String opisTerapije;

        public String OpisTerapije
        {
            get { return opisTerapije; }
            set { opisTerapije = value; OnProperyChanged("OpisTerapije"); }
        }

        
        private Lekar lekarTerapije;

        public Lekar LekarTerapije
        {
            get { return lekarTerapije; }
            set { lekarTerapije = value; OnProperyChanged("LekarTerapije"); }
        }

        private RegistrovaniKorisnik pacijent;

        public RegistrovaniKorisnik Pacijent
        {
            get { return pacijent; }
            set { pacijent = value; OnProperyChanged("Pacijent"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public object Clone()
        {
            return new Terapija
            {
                Sifra = this.Sifra,
                OpisTerapije = this.OpisTerapije,
                LekarTerapije = this.LekarTerapije,     
                Pacijent = this.Pacijent,
                Aktivan = this.Aktivan
            };
        }
        public void SacuvajTerapiju()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Terapija(sifra,opis,lekar,pacijent,aktivan)"
                                    + "VALUES(@sifra,@opis,@lekar,@pacijent,@aktivan)";

                command.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@opis", this.OpisTerapije));
                command.Parameters.Add(new SqlParameter("@lekar", this.LekarTerapije.Korisnik.Jmbg));
                command.Parameters.Add(new SqlParameter("@pacijent", this.Pacijent.Jmbg));
                command.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                command.ExecuteNonQuery();
            }
            Data.Data.Instance.UcitajSveTerapije();
        }
        public void IzmenaTerapije()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update Terapija set opis=@opis,lekar=@lekar,pacijent=@pacijent,aktivan=@aktivan where sifra=@sifra";

                comm.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@opis", this.OpisTerapije));
                comm.Parameters.Add(new SqlParameter("@lekar", this.LekarTerapije.Korisnik.Jmbg));
                comm.Parameters.Add(new SqlParameter("@pacijent", this.Pacijent.Jmbg));
                comm.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Data.Data.Instance.UcitajSveTerapije();
        }

    }

}

