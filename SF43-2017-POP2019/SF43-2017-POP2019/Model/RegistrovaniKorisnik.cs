﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SF43_2017_POP2019.Model
{
    public class RegistrovaniKorisnik : INotifyPropertyChanged, ICloneable
    {
        public enum ETipKorisnika { PACIJENT, ADMINISTRATOR, LEKAR };
        public enum EPol { MUSKI, ZENSKI };


        public RegistrovaniKorisnik()
        {
            aktivan = true;
        }

        private String ime;

        public String Ime
        {
            get { return ime; }
            set { ime = value; OnProperyChanged("Ime"); }
        }

        private String prezime;

        public String Prezime
        {
            get { return prezime; }
            set { prezime = value; OnProperyChanged("Prezime"); }
        }

        private String imePrezime;

        public String ImePrezime
        {
            get { return ime + " " + prezime; }
            set { imePrezime = value; OnProperyChanged("imePrezime"); }
        }

        private String jmbg;

        public String Jmbg
        {
            get { return jmbg; }
            set { jmbg = value; OnProperyChanged("Jmbg"); }
        }

        private String email;

        public String Email
        {
            get { return email; }
            set { email = value; OnProperyChanged("Email"); }
        }

        private Adresa adresa;

        public Adresa Adresa
        {
            get { return adresa; }
            set { adresa = value; OnProperyChanged("Adresa"); }
        }

        private EPol pol;

        public EPol Pol
        {
            get { return pol; }
            set { pol = value; OnProperyChanged("Pol"); }

        }

        private String korisnickoIme;

        public String KorisnickoIme
        {
            get { return korisnickoIme; }
            set { korisnickoIme = value; OnProperyChanged("KorisnickoIme"); }
        }

        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }


        private ETipKorisnika tipK;

        public ETipKorisnika TipK
        {
            get { return tipK; }
            set { tipK = value; OnProperyChanged("TipK"); }
        }

        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }

        }


        public event PropertyChangedEventHandler PropertyChanged;



        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }


      
        public object Clone()
        {
            return new RegistrovaniKorisnik
            {
                Jmbg = this.Jmbg,
                Ime = this.Ime,
                Prezime = this.Prezime,
                ImePrezime = this.ImePrezime,
                Email = this.Email,
                Adresa = this.Adresa,
                KorisnickoIme = this.KorisnickoIme,
                Pol = this.Pol,
                TipK = this.TipK,
                Sifra = this.Sifra,
                Aktivan = this.Aktivan
            };
        }
        public void SacuvajRegistrovanogKorinsika()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO RegistrovaniKorisnik (ime,prezime,email,korisnicko_ime,sifra,adresa, jmbg, tip_korisnika,pol,aktivan)"
                                    + "VALUES(@ime,@prezime,@email,@korisnicko_ime,@sifra,@adresa,@jmbg,@tip_korisnika,@pol,@aktivan)";

                command.Parameters.Add(new SqlParameter("@ime", this.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", this.Prezime));
                command.Parameters.Add(new SqlParameter("@email", this.Email));
                command.Parameters.Add(new SqlParameter("@korisnicko_ime", this.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@adresa", this.Adresa.Id));
                command.Parameters.Add(new SqlParameter("@jmbg", this.Jmbg));
                command.Parameters.Add(new SqlParameter("@tip_korisnika", this.TipK.ToString()));
                command.Parameters.Add(new SqlParameter("@pol", this.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                command.ExecuteNonQuery();
            }
            Data.Data.Instance.UcitajSveRegistrovaneKorisnike();
            Data.Data.Instance.UcitajSvePacijente();

        }
        public void IzmenaRegistrovanogKorisnika()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update RegistrovaniKorisnik set ime=@ime,prezime=@prezime,email=@email,korisnicko_ime=@korisnicko_ime,sifra=@sifra,adresa=@adresa,tip_korisnika=@tip_korisnika,pol=@pol, aktivan=@aktivan where jmbg=@jmbg";

                comm.Parameters.Add(new SqlParameter("@ime", this.Ime));
                comm.Parameters.Add(new SqlParameter("@prezime", this.Prezime));
                comm.Parameters.Add(new SqlParameter("@email", this.Email));
                comm.Parameters.Add(new SqlParameter("@korisnicko_ime", this.KorisnickoIme));
                comm.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@adresa", this.Adresa.Id));
                comm.Parameters.Add(new SqlParameter("@jmbg", this.Jmbg));
                comm.Parameters.Add(new SqlParameter("@tip_korisnika", this.TipK.ToString()));
                comm.Parameters.Add(new SqlParameter("@pol", this.Pol.ToString()));
                comm.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Data.Data.Instance.UcitajSveRegistrovaneKorisnike();
            Data.Data.Instance.UcitajSvePacijente();
        }


    }
}
