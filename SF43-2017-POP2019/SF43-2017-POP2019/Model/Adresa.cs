﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SF43_2017_POP2019.Model
{
    public class Adresa : INotifyPropertyChanged, ICloneable
    {
        public Adresa()
        {
            aktivan = true;
        }

        private String id;

        public String Id
        {
            get { return id; }
            set { id = value; OnProperyChanged("Id"); }
        }

        private String ulica;

        public String Ulica
        {
            get { return ulica; }
            set { ulica = value; OnProperyChanged("Ulica"); }
        }

        private String broj;

        public String Broj
        {
            get { return broj; }
            set { broj = value; OnProperyChanged("Broj"); }
        }

        private String ulicaBroj;

        public String UlicaBroj
        {
            get { return ulica + " " + broj; }
            set { ulicaBroj = value; OnProperyChanged("UlicaBroj"); }
        }




        private String grad;

        public String Grad
        {
            get { return grad; }
            set { grad = value; OnProperyChanged("Grad"); }
        }

        private String drzava;

        public String Drzava
        {
            get { return drzava; }
            set { drzava = value; OnProperyChanged("Drzava"); }
        }

        private bool aktivan;



        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }        

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            return new Adresa
            {
                Id = this.Id,
                Ulica = this.Ulica,
                Broj = this.Broj,
                UlicaBroj = this.UlicaBroj,
                Grad = this.Grad,
                Drzava = this.Drzava,
                Aktivan = this.Aktivan
            };
        }

        public void SacuvajAdresu()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO Adresa(id,ulica,broj,grad,drzava,aktivan)"
                                    + "VALUES(@id,@ulica,@broj,@grad,@drzava,@aktivan)";

                command.Parameters.Add(new SqlParameter("@id", this.Id));
                command.Parameters.Add(new SqlParameter("@ulica", this.Ulica));
                command.Parameters.Add(new SqlParameter("@broj", this.Broj));
                command.Parameters.Add(new SqlParameter("@grad", this.Grad));
                command.Parameters.Add(new SqlParameter("@drzava", this.Drzava));
                command.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));
           

                command.ExecuteNonQuery();
            }
            Data.Data.Instance.UcitajSveAdrese(); 
        }
        public void IzmenaAdrese()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update Adresa set ulica=@ulica,broj=@broj,grad=@grad,drzava=@drzava, aktivan=@aktivan where id=@id";

                comm.Parameters.Add(new SqlParameter("@id", this.Id));
                comm.Parameters.Add(new SqlParameter("@ulica", this.Ulica));
                comm.Parameters.Add(new SqlParameter("@broj", this.Broj));
                comm.Parameters.Add(new SqlParameter("@grad", this.Grad));
                comm.Parameters.Add(new SqlParameter("@drzava", this.Drzava));
                comm.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));                
                

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Data.Data.Instance.UcitajSveAdrese();
        }

    }

}

