﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SF43_2017_POP2019.Model
{
    public class Administrator :  INotifyPropertyChanged, ICloneable
    {
        public Administrator()
        {
            aktivan = true;
        }

        private RegistrovaniKorisnik korisnik;

        public RegistrovaniKorisnik Korisnik
        {
            get { return korisnik; }
            set { korisnik = value; OnProperyChanged("RegistrovaniKorisnik"); }
        }

        private ObservableCollection<DomZdravlja> domoviZdravlja;

        public ObservableCollection<DomZdravlja> DomoviZdravlja
        {
            get { return domoviZdravlja; }
            set { domoviZdravlja = value; OnProperyChanged("DomoviZdravlja"); }
        }
        private ObservableCollection<Termin> termini;

        public ObservableCollection<Termin> Termini
        {
            get { return termini; }
            set { termini = value; OnProperyChanged("Termini"); }
        }

        private ObservableCollection<Terapija> terapije;

        public ObservableCollection<Terapija> Terapije
        {
            get { return terapije; }
            set { terapije = value; OnProperyChanged("Terapije"); }
        }

        private ObservableCollection<RegistrovaniKorisnik> registrovaniKorisnici;

        public ObservableCollection<RegistrovaniKorisnik> RegistrovaniKorisnici
        {
            get { return registrovaniKorisnici; }
            set { registrovaniKorisnici = value; OnProperyChanged("RegistrovaniKorisnici"); }
        }


        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        public object Clone()
        {
            return new Administrator
            {
                Korisnik = this.Korisnik,
                DomoviZdravlja = this.DomoviZdravlja,
                Termini = this.Termini,
                Terapije = this.Terapije,
                RegistrovaniKorisnici = this.RegistrovaniKorisnici,
                Aktivan = this.Aktivan
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
