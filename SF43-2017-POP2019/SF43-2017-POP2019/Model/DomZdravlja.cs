﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SF43_2017_POP2019.Model
{
    public class DomZdravlja : INotifyPropertyChanged, ICloneable
    {
        public DomZdravlja()
        {
            aktivan = true;
        }

        private String sifra;

        public String Sifra
        {
            get { return sifra; }
            set { sifra = value; OnProperyChanged("Sifra"); }
        }

        private String nazivInst;

        public String NazivInst
        {
            get { return nazivInst; }
            set { nazivInst = value; OnProperyChanged("NazivInst"); }
        }

        private Adresa adresa;

        public Adresa Adresa
        {
            get { return adresa; }
            set { adresa = value; OnProperyChanged("Adresa"); }
        }



        private bool aktivan;

        public bool Aktivan
        {
            get { return aktivan; }
            set { aktivan = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public object Clone()
        {
            return new DomZdravlja
            {
                Sifra = this.Sifra,
                NazivInst = this.NazivInst,
                Adresa = this.Adresa,               
                Aktivan = this.Aktivan
            };
        }

        public void SacuvajDomZdravlja()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO DomZdravlja(sifra,naziv_instintuta,adresa,aktivan)"
                                    + "VALUES(@sifra,@naziv_instintuta,@adresa,@aktivan)";

                command.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                command.Parameters.Add(new SqlParameter("@naziv_instintuta", this.NazivInst));
                command.Parameters.Add(new SqlParameter("@adresa", this.Adresa.Id));               
                command.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                command.ExecuteNonQuery();
            }
            Data.Data.Instance.UcitajSveDomoveZdravlja();
        }
        public void IzmenaDomaZdravlja()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "update DomZdravlja set naziv_instintuta=@naziv_instintuta,adresa=@adresa, aktivan=@aktivan where sifra=@sifra";

                comm.Parameters.Add(new SqlParameter("@sifra", this.Sifra));
                comm.Parameters.Add(new SqlParameter("@naziv_instintuta", this.NazivInst));
                comm.Parameters.Add(new SqlParameter("@adresa", this.Adresa.Id));
                comm.Parameters.Add(new SqlParameter("@aktivan", this.Aktivan));


                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Data.Data.Instance.UcitajSveDomoveZdravlja();
        }


    }
}
