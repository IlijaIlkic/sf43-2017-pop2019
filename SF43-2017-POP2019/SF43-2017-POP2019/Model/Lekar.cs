﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;

namespace SF43_2017_POP2019.Model
{
    public class Lekar : INotifyPropertyChanged, ICloneable
    {
        public Lekar()
        {
            korisnik = new RegistrovaniKorisnik();
        }        

        private RegistrovaniKorisnik korisnik;

        public RegistrovaniKorisnik Korisnik
        {
            get { return korisnik; }
            set { korisnik = value; OnProperyChanged("RegistrovaniKorisnik"); }
        }

        private DomZdravlja domZdravlja;

        public DomZdravlja DomZdravlja
        {
            get { return domZdravlja; }
            set { domZdravlja = value; OnProperyChanged("DomZdravlja"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnProperyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public object Clone()
        {
            return new Lekar
            {
                Korisnik = this.Korisnik,
                DomZdravlja = this.DomZdravlja
            };
        }
        public void SacuvajLekara()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"INSERT INTO RegistrovaniKorisnik (ime,prezime,email,korisnicko_ime,sifra,adresa, jmbg, tip_korisnika,pol,aktivan)"
                                    + "VALUES(@ime,@prezime,@email,@korisnicko_ime,@sifra,@adresa,@jmbg,@tip_korisnika,@pol,@aktivan)";

                command.Parameters.Add(new SqlParameter("@ime", this.Korisnik.Ime));
                command.Parameters.Add(new SqlParameter("@prezime", this.Korisnik.Prezime));
                command.Parameters.Add(new SqlParameter("@email", this.Korisnik.Email));
                command.Parameters.Add(new SqlParameter("@korisnicko_ime", this.Korisnik.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@sifra", this.Korisnik.Sifra));
                command.Parameters.Add(new SqlParameter("@adresa", this.Korisnik.Adresa.Id));
                command.Parameters.Add(new SqlParameter("@jmbg", this.Korisnik.Jmbg));
                command.Parameters.Add(new SqlParameter("@tip_korisnika", "LEKAR"));
                command.Parameters.Add(new SqlParameter("@pol", this.Korisnik.Pol.ToString()));
                command.Parameters.Add(new SqlParameter("@aktivan", this.Korisnik.Aktivan));

                command.ExecuteNonQuery();

                command = conn.CreateCommand();

                command.CommandText = @"INSERT INTO Lekar (jmbg,sifra_dom_zdravlja)"
                                    + "VALUES(@jmbg,@sifra_dom_zdravlja)";

                command.Parameters.Add(new SqlParameter("@jmbg", this.Korisnik.Jmbg));
                command.Parameters.Add(new SqlParameter("@sifra_dom_zdravlja", this.DomZdravlja.Sifra));              ;

                command.ExecuteNonQuery();
            }
            Data.Data.Instance.UcitajSveRegistrovaneKorisnike();
            Data.Data.Instance.UcitajSveLekare();
        }
        public void IzmenaLekara()
        {
            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Data.Data.CONNECTION_STRING;
                conn.Open();
                SqlCommand comm = conn.CreateCommand();

                comm.CommandText = "update RegistrovaniKorisnik set ime=@ime,prezime=@prezime,email=@email,korisnicko_ime=@korisnicko_ime,sifra=@sifra,adresa=@adresa,tip_korisnika=@tip_korisnika,pol=@pol, aktivan=@aktivan where jmbg=@jmbg";

                comm.Parameters.Add(new SqlParameter("@ime", this.Korisnik.Ime));
                comm.Parameters.Add(new SqlParameter("@prezime", this.Korisnik.Prezime));
                comm.Parameters.Add(new SqlParameter("@email", this.Korisnik.Email));
                comm.Parameters.Add(new SqlParameter("@korisnicko_ime", this.Korisnik.KorisnickoIme));
                comm.Parameters.Add(new SqlParameter("@sifra", this.Korisnik.Sifra));
                comm.Parameters.Add(new SqlParameter("@adresa", this.Korisnik.Adresa.Id));
                comm.Parameters.Add(new SqlParameter("@jmbg", this.Korisnik.Jmbg));
                comm.Parameters.Add(new SqlParameter("@tip_korisnika", "LEKAR"));
                comm.Parameters.Add(new SqlParameter("@pol", this.Korisnik.Pol.ToString()));
                comm.Parameters.Add(new SqlParameter("@aktivan", this.Korisnik.Aktivan));


                comm.ExecuteNonQuery();

                comm = conn.CreateCommand();
                comm.CommandText = "update Lekar set dom_zdravlja=@dom_zdravlja where jmbg=@jmbg";

                comm.Parameters.Add(new SqlParameter("@jmbg", this.Korisnik.Jmbg));
                comm.Parameters.Add(new SqlParameter("@dom_zdravlja", this.DomZdravlja.Sifra));                

                comm.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Expection {ex}");
            }
            finally
            {
                conn.Close();
            }
            Data.Data.Instance.UcitajSveRegistrovaneKorisnike();
            Data.Data.Instance.UcitajSveLekare();
        }

        public override string ToString()
        {
            return korisnik.ImePrezime;
        }
    }
}
