﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static SF43_2017_POP2019.Model.RegistrovaniKorisnik;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for LekarDodIzmWindow.xaml
    /// </summary>
    public partial class LekarDodIzm : Window
    {
        public enum EOpcija { DODAVANJE, IZMENA }
        private EOpcija opcija;
        private Lekar lekar;
        public LekarDodIzm(Lekar lekar , EOpcija opcija = EOpcija.DODAVANJE)
        {
            InitializeComponent();

            this.lekar = lekar;
            this.opcija = opcija;


            this.DataContext = lekar;

            List<string> sifraDoma = new List<string>();

            foreach (DomZdravlja domZdravlja in Data.Data.Instance.DomoviZdravlja)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = domZdravlja.NazivInst;
                item.Value = domZdravlja.Sifra;
                cbDomZdravlja.Items.Add(item);

                if (lekar.DomZdravlja != null && lekar.DomZdravlja.Sifra == item.Value)
                {
                    cbDomZdravlja.SelectedItem = item;
                }
            }

            cbPol.Items.Add(RegistrovaniKorisnik.EPol.MUSKI);
            cbPol.Items.Add(RegistrovaniKorisnik.EPol.ZENSKI);

            if (opcija.Equals(EOpcija.IZMENA))
            {
                TxtJmbg.IsEnabled = false;
            }

            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = adresa.UlicaBroj;
                item.Value = adresa.Id;

                cbAdresa.Items.Add(item);

                if (lekar.Korisnik!=null && lekar.Korisnik.Adresa != null && lekar.Korisnik.Adresa.Id == item.Value)
                {
                    cbAdresa.SelectedItem = item;
                }
            }

        }

      

        private void btnSacuvaj_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            this.lekar.DomZdravlja = Data.Data.Instance.DomoviZdravlja.First(dz => dz.Sifra == ((ComboboxItem)cbDomZdravlja.SelectedValue).Value);
            this.lekar.Korisnik.Adresa = Data.Data.Instance.Adrese.First(adresa => adresa.Id == ((ComboboxItem)cbAdresa.SelectedValue).Value);
            //this.lekar.TipK = RegistrovaniKorisnik.ETipKorisnika.LEKAR;
            if (opcija.Equals(EOpcija.DODAVANJE))
            {
                
                lekar.SacuvajLekara();
            }

            if (opcija.Equals(EOpcija.IZMENA))
            {
                lekar.IzmenaLekara();
            }
        }

        private void btnOdustani_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
