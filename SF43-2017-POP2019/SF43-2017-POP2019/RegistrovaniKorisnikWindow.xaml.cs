﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for RegistrovaniKorisnikWindow.xaml
    /// </summary>
    public partial class RegistrovaniKorisnikWindow : Window
    {
        ICollectionView view;
        public RegistrovaniKorisnikWindow()
        {
            InitializeComponent();
            DGRegistrovaniKorisnik.ItemsSource = Data.Data.Instance.RegistrovaniKorisnici;
            DGRegistrovaniKorisnik.IsSynchronizedWithCurrentItem = true;

            DGRegistrovaniKorisnik.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.RegistrovaniKorisnici);
            view.Filter = CustomFilter; 
        }

      
        private bool CustomFilter(Object obj)
        {
            RegistrovaniKorisnik registrovaniKorisnik = obj as RegistrovaniKorisnik;
            return txtPretraga.Equals(String.Empty) || registrovaniKorisnik.ImePrezime.Contains(txtPretraga.Text) || registrovaniKorisnik.Email.Contains(txtPretraga.Text) || registrovaniKorisnik.Adresa.UlicaBroj.Contains(txtPretraga.Text) || registrovaniKorisnik.TipK.ToString().Contains(txtPretraga.Text);
        }

        

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

     
       

        

       

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksRegistrovanogKorisnika(String jmbg)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.RegistrovaniKorisnici.Count; i++)
            {
                if (Data.Data.Instance.RegistrovaniKorisnici[i].Jmbg.Equals(jmbg))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanKorisnik(RegistrovaniKorisnik registrovaniKorisnik)
        {
            if (registrovaniKorisnik == null)
            {
                MessageBox.Show("Nije selektovan korisnik! ");
                return false;
            }
            return true;
        }


    }
}
