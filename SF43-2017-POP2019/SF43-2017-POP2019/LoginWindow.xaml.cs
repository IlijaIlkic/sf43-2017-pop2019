﻿using SF43_2017_POP2019.Model;
using SF43_2017_POP2019.Pomocne;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        ICollectionView view;
        public RegistrovaniKorisnik korisnikUlog;
       
        public LoginWindow()
        {
            

            InitializeComponent();
            DGDomZdravlja.ItemsSource = Data.Data.Instance.DomoviZdravlja;
            DGDomZdravlja.IsSynchronizedWithCurrentItem = true;

            foreach (Adresa adresa in Data.Data.Instance.Adrese)
            {
                cbMesto.Items.Add(adresa.Grad);
            }

            foreach (Lekar lekar in Data.Data.Instance.Lekari)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = lekar.Korisnik.ImePrezime;
                item.Value = lekar.DomZdravlja.Sifra;

                cbLekar.Items.Add(item);
            }

            DGDomZdravlja.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.DomoviZdravlja);
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(Object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;
            return (cbLekar.SelectedItem == null || ((ComboboxItem) cbLekar.SelectedItem).Value.Equals(domZdravlja.Sifra)) && (cbMesto.SelectedValue == null || domZdravlja.Adresa.Grad.Equals(cbMesto.SelectedValue));

        }

        private void onLekarChange(object sender, RoutedEventArgs e)
        {
            view.Refresh();
        }

        private void onMestoChange(object sender, RoutedEventArgs e)
        {
            view.Refresh();
        }

        private void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            var provera = 0;
            foreach (RegistrovaniKorisnik registrovaniKorisnik in Data.Data.Instance.RegistrovaniKorisnici)
            {
                if (txtKorisnickoIme.Text.Equals(registrovaniKorisnik.Jmbg) && pswdSifra.Password.Equals(registrovaniKorisnik.Sifra))
                {
                    provera = 1;
                    Data.Data.Instance.ulogovaniKorisnik = registrovaniKorisnik;
                    if (registrovaniKorisnik.TipK.Equals(RegistrovaniKorisnik.ETipKorisnika.ADMINISTRATOR))
                    {
                        AdminGlavniWindow gk = new AdminGlavniWindow();
                        gk.ShowDialog();
                        return;

                    }
                    else if (registrovaniKorisnik.TipK.Equals(RegistrovaniKorisnik.ETipKorisnika.PACIJENT))
                    {
                        KorisnikWindows.PacijentGlavniWindow mw = new KorisnikWindows.PacijentGlavniWindow();
                        mw.ShowDialog();
                        return;
                    }
                    else if (registrovaniKorisnik.TipK.Equals(RegistrovaniKorisnik.ETipKorisnika.LEKAR))
                    {
                        KorisnikWindows.LekarGlavniWindow mw = new KorisnikWindows.LekarGlavniWindow();
                        mw.ShowDialog();
                        return;
                        
                    }

                }
            }
            if (provera == 0)
            {
                MessageBox.Show("Niste dobro uneli podatke");
            }

        }

        private void btnRegistracija_Click(object sender, RoutedEventArgs e)
        {
            KorisnikWindows.RegistracijaWindow ad = new KorisnikWindows.RegistracijaWindow();
            ad.ShowDialog();
            
        }

    }
}
