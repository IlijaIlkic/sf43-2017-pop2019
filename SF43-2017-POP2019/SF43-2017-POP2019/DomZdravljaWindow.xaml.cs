﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for DomZdravljaWindow.xaml
    /// </summary>
    public partial class DomZdravljaWindow : Window
    {
        ICollectionView view;
        public DomZdravljaWindow()
        {
            InitializeComponent();
            DGDomZdravlja.ItemsSource = Data.Data.Instance.DomoviZdravlja;
            DGDomZdravlja.IsSynchronizedWithCurrentItem = true;

            DGDomZdravlja.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(Data.Data.Instance.DomoviZdravlja);
            

            if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.ADMINISTRATOR)
            {
                cbMesto.Visibility = Visibility.Hidden;
                view.Filter = CustomFilter;
            }
            else 
            {
                BtnDodajDomZdravlja.Visibility = Visibility.Hidden;
                BtnIzmeniDomZdravlja.Visibility = Visibility.Hidden;
                BtnObrisiDomZdravlja.Visibility = Visibility.Hidden;
                view.Filter = CustomFilterMesto;

                columnAktivan.Visibility = Visibility.Hidden;
                foreach (Adresa adresa in Data.Data.Instance.Adrese)
                {
                    cbMesto.Items.Add(adresa.Grad);
                }
            }
            
        }
        private bool CustomFilter(Object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;
            return txtPretraga.Equals(String.Empty) || domZdravlja.NazivInst.Contains(txtPretraga.Text) || domZdravlja.Adresa.UlicaBroj.Contains(txtPretraga.Text);
   
        }

        private bool CustomFilterMesto(Object obj)
        {
            DomZdravlja domZdravlja = obj as DomZdravlja;
            return cbMesto.SelectedValue == null || domZdravlja.Adresa.Grad.Equals(cbMesto.SelectedValue);

        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void onMestoChange(object sender, RoutedEventArgs e)
        {
            view.Refresh();
        }

        private void btnDodajDomZdravlja_Click(object sender, RoutedEventArgs e)
        {
       
            DomZdravljaDodIzm ad = new DomZdravljaDodIzm(new DomZdravlja());
            ad.ShowDialog();
           
        }

        private void btnIzmeniDomZdravlja_Click(object sender, RoutedEventArgs e)
        { 
            DomZdravlja selektovanDomZdravlja = (DomZdravlja)DGDomZdravlja.SelectedItem;
            if (selektovanDomZdravlja != null)
            {
                DomZdravlja stari = (DomZdravlja)selektovanDomZdravlja.Clone();
                DomZdravljaDodIzm ai = new DomZdravljaDodIzm(selektovanDomZdravlja, DomZdravljaDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksDomZdravlja(selektovanDomZdravlja.Sifra.ToString());
                    Data.Data.Instance.DomoviZdravlja[indeks] = stari;
                }
                else
                {
                    selektovanDomZdravlja.IzmenaDomaZdravlja();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovana nijedna adresa");
            }
            DGDomZdravlja.Items.Refresh();   
        }

        private void btnObrisiDomZdravlja_Click(object sender, RoutedEventArgs e)
        {
            DomZdravlja domZdravlja= (DomZdravlja)DGDomZdravlja.SelectedItem;
            if (SelektovanDomZdravlja(domZdravlja) && domZdravlja.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksDomZdravlja(domZdravlja.Sifra.ToString());
                    Data.Data.Instance.DomoviZdravlja[indeks].Aktivan = false;
                    DomZdravlja SelektovanDomZdravlja = (DomZdravlja)DGDomZdravlja.SelectedItem;
                    Data.Data.Instance.DomoviZdravlja.Remove(SelektovanDomZdravlja);

                    SelektovanDomZdravlja.IzmenaDomaZdravlja();
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksDomZdravlja(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.DomoviZdravlja.Count; i++)
            {
                if (Data.Data.Instance.DomoviZdravlja[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanDomZdravlja(DomZdravlja domZdravlja)
        {
            if (domZdravlja == null)
            {
                MessageBox.Show("Nije selektovan dom zdravlja! ");
                return false;
            }
            return true;
        }


    }
}

        