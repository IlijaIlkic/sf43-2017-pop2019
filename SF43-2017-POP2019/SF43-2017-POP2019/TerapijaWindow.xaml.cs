﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for Terapija.xaml
    /// </summary>
    public partial class TerapijaWindow : Window
    {
        ICollectionView view;
        public TerapijaWindow()
        {
            InitializeComponent();
            if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.PACIJENT)
            {
                DGTerapija.ItemsSource = Data.Data.Instance.Terapije.Where(terapija => terapija.Pacijent.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg && terapija.Aktivan);
                columnAktivan.Visibility = Visibility.Hidden;
                BtnDodajTerapiju.Visibility = Visibility.Hidden;
                BtnIzmeniTerapiju.Visibility = Visibility.Hidden;
                BtnObrisiTerapiju.Visibility = Visibility.Hidden;
            }
            else if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.ADMINISTRATOR)
            {
                DGTerapija.ItemsSource = Data.Data.Instance.Terapije;
            }
            else if (Data.Data.Instance.ulogovaniKorisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.LEKAR)
            {
                DGTerapija.ItemsSource = Data.Data.Instance.Terapije.Where(terapija => terapija.LekarTerapije.Korisnik.Jmbg == Data.Data.Instance.ulogovaniKorisnik.Jmbg && terapija.Aktivan);
                BtnIzmeniTerapiju.Visibility = Visibility.Hidden;
                BtnObrisiTerapiju.Visibility = Visibility.Hidden;
            }
            DGTerapija.IsSynchronizedWithCurrentItem = true;

            DGTerapija.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            view = CollectionViewSource.GetDefaultView(DGTerapija.ItemsSource);
            view.Filter = CustomFilter;
        }
        private bool CustomFilter(Object obj)
          {
            Terapija terapija= obj as Terapija;
            return txtPretraga.Equals(String.Empty) || terapija.OpisTerapije.Contains(txtPretraga.Text) || terapija.LekarTerapije.Korisnik.ImePrezime.Contains(txtPretraga.Text);

        }

        private void txtPretraga_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }

        private void btnDodajTerapiju_Click(object sender, RoutedEventArgs e)
        {
            TerapijaDodIzm ad = new TerapijaDodIzm(new Terapija());
            ad.ShowDialog();
        }

        private void btnIzmeniTerapiju_Click(object sender, RoutedEventArgs e)
        {
            Terapija selektovanaTerapija = (Terapija)DGTerapija.SelectedItem;
            if (selektovanaTerapija != null)
            {
                Terapija stari = (Terapija)selektovanaTerapija.Clone();
                TerapijaDodIzm ai = new TerapijaDodIzm(selektovanaTerapija, TerapijaDodIzm.EOpcija.IZMENA);
                if (ai.ShowDialog() != true)
                {
                    int indeks = IndeksTerapija(selektovanaTerapija.Sifra.ToString());
                    Data.Data.Instance.Terapije[indeks] = stari;
                }
                else
                {
                    selektovanaTerapija.IzmenaTerapije();
                }

            }
            else
            {
                MessageBox.Show("Nije selektovana nijedna terapija");
            }
            DGTerapija.Items.Refresh();
        }

        private void btnObrisiTerapiju_Click(object sender, RoutedEventArgs e)
        {
            Terapija terapija = (Terapija)DGTerapija.SelectedItem;
            if (SelektovanaTerapija(terapija) && terapija.Aktivan)
            {
                if (MessageBox.Show("Da li ste sigurni?", "Potvrda", MessageBoxButton.YesNo).Equals(MessageBoxResult.Yes))
                {
                    int indeks = IndeksTerapija(terapija.Sifra.ToString());
                    Data.Data.Instance.Terapije[indeks].Aktivan = false;
                    Terapija selektovanaTerapija = (Terapija)DGTerapija.SelectedItem;
                    Data.Data.Instance.Terapije.Remove(selektovanaTerapija);

                    selektovanaTerapija.IzmenaTerapije();
                }
            }
        }

        private void btnNazad_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private int IndeksTerapija(String sifra)
        {
            int indeks = -1;
            for (int i = 0; i < Data.Data.Instance.Terapije.Count; i++)
            {
                if (Data.Data.Instance.Terapije[i].Sifra.Equals(sifra))
                {
                    indeks = i;
                    break;
                }
            }
            return indeks;
        }

        private bool SelektovanaTerapija(Terapija terapija)
        {
            if (terapija == null)
            {
                MessageBox.Show("Nije selektovana terapija! ");
                return false;
            }
            return true;
        }


    }
}