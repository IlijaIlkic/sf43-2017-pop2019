﻿using SF43_2017_POP2019.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SF43_2017_POP2019.Data
{
    class Data
    {
        public ObservableCollection<Termin> Termini { get; set; }
        public ObservableCollection<Terapija> Terapije { get; set; }
        public ObservableCollection<RegistrovaniKorisnik> RegistrovaniKorisnici{ get; set; }
        public ObservableCollection<DomZdravlja> DomoviZdravlja{ get; set; }
        public ObservableCollection<Adresa> Adrese{ get; set; }
        public ObservableCollection<RegistrovaniKorisnik> Pacijenti { get; set; }
        public ObservableCollection<Lekar> Lekari { get; set; }
        public ObservableCollection<RegistrovaniKorisnik> Administratori { get; set; }
        public RegistrovaniKorisnik ulogovaniKorisnik;


        public string ConnectionString
    {
        get
        {
            return CONNECTION_STRING;
        }
    }

    public const String CONNECTION_STRING = @"Data Source=localhost;Initial Catalog=dom_zdravlja;Integrated Security=True";


        private Data()
        {

            Termini = new ObservableCollection<Termin>();
            Terapije = new ObservableCollection<Terapija>();
            RegistrovaniKorisnici = new ObservableCollection<RegistrovaniKorisnik>();
            DomoviZdravlja = new ObservableCollection<DomZdravlja>();
            Adrese = new ObservableCollection<Adresa>();
            Lekari = new ObservableCollection<Lekar>();
            Pacijenti = new ObservableCollection<RegistrovaniKorisnik>();
            Administratori = new ObservableCollection<RegistrovaniKorisnik>();
            UcitajSveAdrese();
            UcitajSveRegistrovaneKorisnike();
            UcitajSveDomoveZdravlja();
            UcitajSvePacijente();
            UcitajSveLekare();
            UcitajSveAdmine();
            UcitajSveTerapije();
            UcitajSveTermine();
            

        }

        private static Data _instance = null;

        public static Data Instance
        {

            get
            {
                if (_instance == null)
                    _instance = new Data();
                return _instance;
            }
        }

        public void UcitajSveAdrese()
        {
            Adrese.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Adresa";

                SqlDataAdapter daAdrese = new SqlDataAdapter();
                daAdrese.SelectCommand = command;

                DataSet dsAdrese = new DataSet();
                daAdrese.Fill(dsAdrese, "Adresa");

                foreach (DataRow row in dsAdrese.Tables["Adresa"].Rows)
                {
                    Adresa adresa= new Adresa();

                    adresa.Id = (string)row["id"];
                    adresa.Ulica = (string)row["ulica"];
                    adresa.Broj = (string)row["broj"];
                    adresa.Grad = (string)row["grad"];
                    adresa.Drzava = (string)row["drzava"];
                    adresa.Aktivan = (bool)row["aktivan"];

                    Adrese.Add(adresa);
                }
            }
        }
        public void UcitajSveTerapije()
        {
            Terapije.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Terapija";

                SqlDataAdapter daTerapije = new SqlDataAdapter();
                daTerapije.SelectCommand = command;

                DataSet dsTerapije = new DataSet();
                daTerapije.Fill(dsTerapije, "Terapija");

                foreach (DataRow row in dsTerapije.Tables["Terapija"].Rows)
                {
                    Terapija terapija = new Terapija();

                    terapija.Sifra = (string)row["sifra"];
                    terapija.OpisTerapije = (string)row["opis"];
                    terapija.LekarTerapije = (Lekar)Lekari.First(lekar => lekar.Korisnik.Jmbg == (string)row["lekar"]);
                    terapija.Aktivan = (bool)row["aktivan"];
                    terapija.Pacijent = (RegistrovaniKorisnik)Pacijenti.First(pacijent => pacijent.Jmbg == (string)row["pacijent"]);

                    Terapije.Add(terapija);
                }
            }
        }
        public void UcitajSveDomoveZdravlja()
        {
            DomoviZdravlja.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM DomZdravlja";

                SqlDataAdapter daDomZdravlja = new SqlDataAdapter();
                daDomZdravlja.SelectCommand = command;

                DataSet dsDomdravlja = new DataSet();
                daDomZdravlja.Fill(dsDomdravlja, "DomZdravlja");

                foreach (DataRow row in dsDomdravlja.Tables["DomZdravlja"].Rows)
                {
                    DomZdravlja domZdravlja= new DomZdravlja();

                    domZdravlja.Sifra = (string)row["sifra"];
                    domZdravlja.NazivInst = (string)row["naziv_instintuta"];
                    domZdravlja.Adresa = Adrese.First(adresa => adresa.Id == (string)row["adresa"]);
                    domZdravlja.Aktivan = (bool)row["aktivan"];


                    DomoviZdravlja.Add(domZdravlja);
                }
            }
        }
        public void UcitajSveLekare()
        {
            Lekari.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Lekar";

                SqlDataAdapter daLekar = new SqlDataAdapter();
                daLekar.SelectCommand = command;

                DataSet dsLekar = new DataSet();
                daLekar.Fill(dsLekar, "Lekar");

                foreach (DataRow row in dsLekar.Tables["Lekar"].Rows)
                {
                    Lekar lekar = new Lekar();
                    lekar.Korisnik = RegistrovaniKorisnici.First(korisnik => korisnik.Jmbg == (string)row["jmbg"]);  
                    lekar.DomZdravlja = DomoviZdravlja.First(domZdravlja => domZdravlja.Sifra == (string)row["sifra_dom_zdravlja"]);
                    Lekari.Add(lekar);
                }
            }
        }
        public void UcitajSveAdmine()
        {
            Administratori.Clear();
            foreach (RegistrovaniKorisnik korisnik in RegistrovaniKorisnici)
            {
                if (korisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.ADMINISTRATOR)
                {
                    Administratori.Add(korisnik);
                }
            }
        }


        public void UcitajSvePacijente()
        {
            Pacijenti.Clear();
            foreach (RegistrovaniKorisnik korisnik in RegistrovaniKorisnici)
            {
                if (korisnik.TipK == RegistrovaniKorisnik.ETipKorisnika.PACIJENT)
                {
                    Pacijenti.Add(korisnik);
                }
            }
        }

        public void UcitajSveRegistrovaneKorisnike()
        {
            RegistrovaniKorisnici.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM RegistrovaniKorisnik";

                SqlDataAdapter daRegistrovaniKorisnik = new SqlDataAdapter();
                daRegistrovaniKorisnik.SelectCommand = command;

                DataSet dsRegistrovaniKorisnik = new DataSet();
                daRegistrovaniKorisnik.Fill(dsRegistrovaniKorisnik, "RegistrovaniKorisnik");

                foreach (DataRow row in dsRegistrovaniKorisnik.Tables["RegistrovaniKorisnik"].Rows)
                {
                    RegistrovaniKorisnik registrovaniKorisnik = new RegistrovaniKorisnik();

                    registrovaniKorisnik.Ime = (string)row["ime"];
                    registrovaniKorisnik.Prezime = (string)row["prezime"];
                    registrovaniKorisnik.Email = (string)row["email"];
                    registrovaniKorisnik.Adresa = (Adresa)Adrese.First(adresa => adresa.Id == (string)row["adresa"]);
                    registrovaniKorisnik.KorisnickoIme = (string)row["korisnicko_ime"];
                    registrovaniKorisnik.Sifra = (string)row["sifra"];
                    registrovaniKorisnik.Pol = (RegistrovaniKorisnik.EPol)Enum.Parse(typeof(RegistrovaniKorisnik.EPol), row["pol"].ToString());
                    registrovaniKorisnik.TipK = (RegistrovaniKorisnik.ETipKorisnika)Enum.Parse(typeof(RegistrovaniKorisnik.ETipKorisnika), row["tip_korisnika"].ToString());
                    registrovaniKorisnik.Jmbg = (string)row["jmbg"];                    
                    registrovaniKorisnik.Aktivan = (bool)row["aktivan"];


                    RegistrovaniKorisnici.Add(registrovaniKorisnik);
                }
            }
        }
        public void UcitajSveTermine()
        {
            Termini.Clear();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = CONNECTION_STRING;
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT * FROM Termin";

                SqlDataAdapter daTermini = new SqlDataAdapter();
                daTermini.SelectCommand = command;

                DataSet dsTerapije = new DataSet();
                daTermini.Fill(dsTerapije, "Termin");

                foreach (DataRow row in dsTerapije.Tables["Termin"].Rows)
                {
                    Termin termin = new Termin();

                    termin.Sifra = (string)row["sifra"];
                    termin.LekarZakazan = (Lekar)Lekari.First(lekar => lekar.Korisnik.Jmbg == (string)row["lekar"]);
                    termin.DatumTermina = (DateTime)row["datum"];
                    termin.Pacijent = (RegistrovaniKorisnik)RegistrovaniKorisnici.First(korisnik => korisnik.Jmbg  == (string)row["pacijent"]);
                    termin.StatusTermina = (Termin.EStatusTermina)Enum.Parse(typeof(Termin.EStatusTermina), row["status"].ToString());
                     termin.Aktivan = (bool)row["Aktivan"]; 


                    Termini.Add(termin);
                }
            }
        }

    }
}
