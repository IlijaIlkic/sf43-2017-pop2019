﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF43_2017_POP2019
{
    /// <summary>
    /// Interaction logic for AdminGlavniWindow.xaml
    /// </summary>
    public partial class AdminGlavniWindow : Window
    {
        public AdminGlavniWindow()
        {
            InitializeComponent();
            txtkorisnickoIme.Text = Data.Data.Instance.ulogovaniKorisnik.ImePrezime;

        }



        private void btnAdrese_Click(object sender, RoutedEventArgs e)
        {
            AdresaWindow ak = new AdresaWindow();
            ak.ShowDialog();
        }

      

       
        private void btnTermini_Click(object sender, RoutedEventArgs e)
        {
            TerminiWindow ak = new TerminiWindow();
            ak.ShowDialog();
        }

        private void btnTerapije_Click(object sender, RoutedEventArgs e)
        {
            TerapijaWindow ak = new TerapijaWindow();
            ak.ShowDialog();
        }

        private void btnDomZdravlja_Click(object sender, RoutedEventArgs e)
        {
            DomZdravljaWindow ak = new DomZdravljaWindow();
            ak.ShowDialog();
        }

        private void btnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            RegistrovaniKorisnikWindow ak = new RegistrovaniKorisnikWindow();
            ak.ShowDialog();
        }

        private void btnPacijenti_Click(object sender, RoutedEventArgs e)
        {
            PacijentWindow ak = new PacijentWindow();
            ak.ShowDialog();

        }

        private void btnLekari_Click(object sender, RoutedEventArgs e)
        {
            LekarWindow ak = new LekarWindow();
            ak.ShowDialog();


        }

        private void btnAdmini_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow ak = new AdminWindow();
            ak.ShowDialog();
        }
    }
}
